const faker = require('faker')
const Realm = require('realm')
const fs = require('fs')
const readline = require('readline');
var colors = require('colors');

var totalTickers = 100
var username = "realm-admin"
var password = ""
var URL = "127.0.0.1:9080"
var _realm;

const TickerSchema = {
  name: 'Ticker',
  primaryKey: 'tickerSymbol',
  properties: {
    tickerSymbol: { type: 'string', optional: false },
    price: { type: 'int', optional: false },
    companyName: { type: 'string', optional: false },
  }
}

var token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJOb3RpZmllciI6dHJ1ZSwiU3luYyI6dHJ1ZSwiQ2xpZW50RW1haWwiOiJkbWNraW5uZXlAYXRsYXNyZmlkLmNvbSIsImlhdCI6MTUxODY0MTU4NywiZXhwIjoxNTE5ODUxMTg3fQ.NSRATXZgp0aBCodBS_oiCOHcJkswcXGQMd14Tc85jV8gG7liQrdToOlVshQ8YHr90nbulZrMGR5_Nf9YNf42Hqr1dNlkra6v89tsP8BGKJ_W_XgySnC1YR9cWPXJHLuqTD7Pj3K-o0SD6hsHW_O0omkBOtlqV6HUGyE4O6my_xLiGfDRjY_S48H79phmx-N-j8OD6JZqbKIqT8w07PTU-aWwdDXitMSnzzpPx-QL-qO01mC5ztszPPX2aurVkCdXnyZIZRHdrlsS8YsDk2TtlqUraBw66icKp08Kjti7FcgUj2kYwcKnnGeOyJjeKdToWwZrtTNOBOvqFJ-1juJZMg";

// Unlock Professional Edition APIs
Realm.Sync.setFeatureToken(token);

function generateRandomTickerSymbol(len) {
  charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  let randomString = '';
  for (let i = 0; i < len; i++) {
    let randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
}
function loginRealm () {
    return Realm.Sync.User.login(`http://${URL}`, username, password);               
}

function getStarted() {
   
    loginRealm().then(user => {              
        Realm.open({
        sync: {
            url: `realm://${URL}/tickers`,
            user: user,
            error: (sender, error) => {                
                console.log(sender);                    
            }
        },
        schema: [TickerSchema],
    }).then(realm => {
            _realm = realm;       
            let tickerResults = realm.objects('Ticker')
            // Add some data to the tickers Realms
            if (tickerResults.length < totalTickers) {
                realm.write(() => {
                    for (let index = 0; index < totalTickers; index++) {
                        realm.create('Ticker', {
                            tickerSymbol: generateRandomTickerSymbol(3),
                            price: index,
                            companyName: faker.company.companyName()
                        }, true)
                    }
                })
            }
                        
            realm.syncSession.addProgressNotification('download', 'reportIndefinitely', (transfered, transferable) => {
                console.log("Download: " + transfered / transferable);
            })

            realm.syncSession.addProgressNotification('upload', 'reportIndefinitely', (transfered, transferable) => {
                console.log("Upload: " + transfered / transferable);
            })
            
            askQuestion()
            tickerResults.addListener((objects, changes) => {
                changes.modifications.forEach((index) => {
                    var ticker = objects[index];
                 //   console.log(colors.red(`CHANGES Ticker ${ticker.companyName} - ${ticker.tickerSymbol} - ${ticker.price}`))                                
                });
            }) 
    })});  
}

function askQuestion() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
      });
      
    rl.question('Enter <ticker> <value> to change or enter list <ticker> to list ticker value:', (answer) => {           
       
    if (answer.startsWith('list')) {
        var symbol = answer.split(' ')[1];        
        var ticker = _realm.objects('Ticker').filtered('tickerSymbol = "' + symbol + '"')[0];

        console.log(colors.yellow('Symbol: ' + ticker.tickerSymbol + ' Price: ' + ticker.price + ' Company: ' + ticker.companyName)); 
    } else {
        var split = answer.split(' ');
        var symbol = split[0].trim();
        var value = split[1]
        
        var ticker = _realm.objects('Ticker').filtered('tickerSymbol = "' + symbol + '"')[0];
        
        _realm.write(() => {
            ticker.price = +value;
        });

    }

    rl.close();
    askQuestion();
});      
}

getStarted();
